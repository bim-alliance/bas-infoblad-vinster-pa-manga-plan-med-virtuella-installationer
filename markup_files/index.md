![Bild 1](media/1.jpeg)

**Ritning med installationsdel i 2D, samma i 3D-modell och slutlig utformning i verkligheten.**

# Vinster på många plan med virtuella installationer

> ##### Att arbeta med BIM i installationsbranschen ger många fördelar under hela processen, från projektering till drift och underhåll. Men än så länge har användningen av BIM skett fläckvis över landet. När nu projektet Virtuella installationer avslutats finns en god plattform att stå på för att komma igång med BIM-arbetet. Förutsättningarna för att BIM på ett brett plan ska få ett genomslag i installationsbranschen har aldrig varit bättre.

UNDER 2014 SLUTFÖRDES DET BRANSCHGEMENSAMMA SBUF-projektet Virtuella installationer med syfte att ge installatörer stöd för att komma igång med BIM och dra nytta av information från CAD-system. Det konkreta projektresultatet är sju dokument
som finns på VVS Företagens hemsida och som gör det möjligt för installationsföretagen att effektivisera sin verksamhet. Dokumenten ger stöd till installatören vid upphandling och genomförande av en totalentreprenad med BIM.
​	För att få upp intresset för BIM och 3D bland installatörer genomfördes tidigare, 2011-2012, SBUF-projektet BIM för installatörer/Virtuell produktionsplanering. Projektet utmynnade i en film och en vision om hur BIM-arbete kan bedrivas. Utvecklingsarbetet med BIM för installatörer fortsatte därefter med projektet Virtuella installationer. 
​	Under projekttiden har ett 90-tal intervjuer med cirka 200 personer i olika roller ägt rum för att ge projektgruppen en bild av problem och möjligheter. Dessutom har det genomförts ett antal seminarier med olika gruppkonstellationer. BIM Alliance installationsgrupp har spelat en viktig roll i arbetet liksom branschens företag samt projektet BIP (Building Information Properties) för installationer som pågått parallellt. Att det finns gemensamma beteckningar för de olika komponenterna är en viktig förutsättning för att virtuella installationer ska fungera.
​	– Fokus i projektet har varit installatörens situation och vi har försökt skriva allt utifrån installatörens förutsättningar, säger Carl-Erik Brohn, som arbetat i projektets styrgrupp. För att BIM ska fungera krävs en samverkan med projektören – det som
inte ritas finns inte. Ytterst är det beställaren som måste se nyttan med en mer detaljerad projektering och få någon form av indikation från installatören om att det totalt sett blir billigare om projektören gör en del merarbete.
​	Ju fler beställare som har en helhetssyn desto bättre blir det. Långsiktiga fastighetsägare har nytta av att se helheten och där finns intresse för BIM och de fördelar det ger till drift och underhåll.

I EN ÖVERSIKT GES EN INTRODUKTION till vad BIM är och hur BIM kan användas vid idé, projektering, produktion och förvaltning. Här presenteras även vad de olika hjälpmedlen innehåller.
​	Handledningen ger anvisningar till hur man skapar en BIMmanual och är ett hjälpmedel när installatörer, projektörer och byggherrar ska komma överens om hur BIM ska användas i ett projekt. Installatören och projektören måste först bestämma
vilka programvaror de vill jobba med. BIM-manualen är en mall som fylls i när man arbetar med handledningen. Resultatet är en manual för installationsarbetet vid ett specifikt projekt.
​	– BIM-manualen ska egentligen skapas av byggherren i ett tidigt skede men ibland sker inte detta, säger Hans Söderström, projektutvecklare på Imtech VS Teknik. Projektet visar vad man måste ha med i en manual för att effektivare kunna
jobba med installationerna i planering och produktion. 

TRE DOKUMENT BEHANDLAR leveransspecifikationer med BIP inarbetat och innehåller olika mallar för ventilation, VS rör och El tele speciellt för information till kalkyl.
​	– Det är hela tiden en avvägning av vad som görs manuellt och vad som görs i systemen. Leveransspecifikationen kan även användas när man gör en kravspec för att få information till inköp, logistik eller till drift och förvaltning. Vi har försökt
göra detta så heltäckande som möjligt, säger Carl-Erik Brohn. 
​	I en bilagedel beskrivs stöd för upphandling samt ges förklaringar till relevanta begrepp. Dessutom finns en processbeskrivning för en installatör. Här kan installatören se möjligheterna med BIM och projektören få en bild av hur installatören kan arbeta och utifrån det fundera över vad man som projektör kan bidra med.

EN YRKESKUNNIG INSTALLATÖR, kalkylator eller projektör bör ha stor hjälp av de olika hjälpmedlen för att komma igång med BIM-arbete, men för att lyckas måste man jobba i team och ha med sig drift och förvaltning så tidigt som möjligt i processen.
Både Hans Söderström och Carl-Erik Brohn poängterar att om man inte jobbat med BIM-projekt tidigare måste man ha hjälp av en BIM-samordnare och gärna en BIM-strateg.
​	– På lite längre sikt måste projektledare, projekteringsledare och andra idag befintliga funktioner lära sig att arbeta utifrån BIM så att detta arbetssätt blir en naturlig del i ett projekt, säger Carl-Erik Brohn.
​	En stor fördel med BIM i installationsbranschen är att det blir enkelt att överföra mängder till kalkyler och andra produktionssystem för planering, inköp, logistik med mera. En annan fördel är att det blir lättare att undvika kollisioner mellan installationer och byggnad och att montörerna har ett bra verktyg där kan de kan se installationer och byggnadsdelar i 3D. Att kunna ge korrekt och uppdaterad information till drift
och förvaltning är ytterligare exempel på fördelar med BIM. Dessutom – BIM innebär minskad total kostnad och tid, bättre arbetsmiljö och bättre kvalitet!

DE DOKUMENT SOM NU FINNS på VVS Företagens hemsida är inte en slutprodukt utan en plattform för fortsatt arbete. Förhoppningsvis kan materialet även bli ett bra hjälpmedel i utbildningar av installatörer och projektörer. Synpunkter på de olika
delarna tas tacksamt emot och blir viktig input vid kommande uppdateringar av materialet.
​	För att BIM ska slå igenom på installationssidan krävs en bra helhetssyn där parterna är öppna mot varandra. Ur ett företagsperspektiv gäller att visa på de vinster som arbete med BIM leder till.
​	– Alla programvaror kan inte hantera BIM men nu börjar de som gör CAD-program och de som gör kalkylprogram att förstå varandra och vi kan förvänta oss bättre programvaror framöver, säger Hans Söderström. Vi måste även få fram uppdaterade programvaror som klarar att ha hand om informationen utan att alla ska behöva ha CAD-licenser. För att lyckas måste vi också locka unga personer med datavana till branschen. Branschen befinner sig i ett generationsskifte och måste fyllas på med nytt folk. BIM är rätt i tiden! 